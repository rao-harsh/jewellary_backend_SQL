'use strict';
module.exports = function(app) {
  var Controller = require('./appController');

  // Routes  : API  
  app.get('/api/demo', Controller.demo);

  app.get('/api/get_shopify_products', Controller.getShopifyProducts);
  app.get('/api/get_shopify_customers', Controller.getShopifyCustomers);
  app.get('/api/get_shopify_collections', Controller.getShopifyCollections);
  app.get('/api/get_shopify_orders', Controller.getShopifyOrders);

  // All Webhooks 
  app.all('/api/collection_create_webhook', Controller.collectionCreateWebhook);
  app.all('/api/collection_update_webhook', Controller.collectionUpdateWebhook);
  app.all('/api/collection_delete_webhook', Controller.collectionDeleteWebhook);

  app.all('/api/customer_create_webhook', Controller.customerCreateWebhook);
  app.all('/api/customer_delete_webhook', Controller.customerDeleteWebhook);
  app.all('/api/customer_update_webhook', Controller.customerUpdateWebhook);

  app.all('/api/product_create_webhook', Controller.productCreateWebhook);
  app.all('/api/product_delete_webhook', Controller.productDeleteWebhook);
  app.all('/api/product_update_webhook', Controller.productUpdateWebhook);
  
  app.all('/api/order_create_webhook', Controller.orderCreateWebhook);
  app.all('/api/order_delete_webhook', Controller.orderDeleteWebhook);
  app.all('/api/order_update_webhook', Controller.orderUpdateWebhook);


};