'use strict';

var Model = require('./appModel');
const util = require('util');
var fs 	= require('fs');
var nodemailer = require('nodemailer');


exports.demo = function(req, res) {
	res.status(200).send({ success:true, message: 'This is demo!' });
};

// Fetch Shopify Data from Store
exports.getShopifyProducts = function(req, res) {
	Model.getShopifyProductsModel(req, function(err, data) {
		if (err)  res.send(err);
		if(data && data.success){
			res.status(200).send({ success:true, message: data.msg, data: data.data });
		}			
		else{
			res.status(200).send({ success:false, message: data.msg });
		}
	});
};

exports.getShopifyCustomers = function(req, res) {
	Model.getShopifyCustomersModel(req, function(err, data) {
		if (err)  res.send(err);
		if(data && data.success){
			res.status(200).send({ success:true, message: data.msg, data: data.data });
		}			
		else{
			res.status(200).send({ success:false, message: data.msg });
		}
	});
};

exports.getShopifyCollections = function(req, res) {
	Model.getShopifyCollectionsModel(req, function(err, data) {
		if (err)  res.send(err);
		if(data && data.success){
			res.status(200).send({ success:true, message: data.msg, data: data.data });
		}			
		else{
			res.status(200).send({ success:false, message: data.msg });
		}
	});
};

exports.getShopifyOrders = function(req, res) {
	Model.getShopifyOrdersModel(req, function(err, data) {
		if (err)  res.send(err);
		if(data && data.success){
			res.status(200).send({ success:true, message: data.msg, data: data.data });
		}			
		else{
			res.status(200).send({ success:false, message: data.msg });
		}
	});
};

// Shopify WebHooks

exports.collectionCreateWebhook = function(req, res) {
	Model.collectionCreateWebhookModel(req, function(err, data) {
		if (err)  res.send(err);
		if(data && data.success){
			res.status(200).send({ success:true, message: data.msg, data: data.data });
		}			
		else{
			res.status(200).send({ success:false, message: data.msg });
		}
	});
};

exports.collectionUpdateWebhook = function(req, res) {
	Model.collectionUpdateWebhookModel(req, function(err, data) {
		if (err)  res.send(err);
		if(data && data.success){
			res.status(200).send({ success:true, message: data.msg, data: data.data });
		}			
		else{
			res.status(200).send({ success:false, message: data.msg });
		}
	});
};

exports.collectionDeleteWebhook = function(req, res) {
	Model.collectionDeleteWebhookModel(req, function(err, data) {
		if (err)  res.send(err);
		if(data && data.success){
			res.status(200).send({ success:true, message: data.msg, data: data.data });
		}			
		else{
			res.status(200).send({ success:false, message: data.msg });
		}
	});
};

exports.customerCreateWebhook = function(req, res) {
	Model.customerCreateWebhookModel(req, function(err, data) {
		if (err)  res.send(err);
		if(data && data.success){
			res.status(200).send({ success:true, message: data.msg, data: data.data });
		}			
		else{
			res.status(200).send({ success:false, message: data.msg });
		}
	});
};

exports.customerUpdateWebhook = function(req, res) {
	Model.customerUpdateWebhookModel(req, function(err, data) {
		if (err)  res.send(err);
		if(data && data.success){
			res.status(200).send({ success:true, message: data.msg, data: data.data });
		}			
		else{
			res.status(200).send({ success:false, message: data.msg });
		}
	});
};

exports.customerDeleteWebhook = function(req, res) {
	Model.customerDeleteWebhookModel(req, function(err, data) {
		if (err)  res.send(err);
		if(data && data.success){
			res.status(200).send({ success:true, message: data.msg, data: data.data });
		}			
		else{
			res.status(200).send({ success:false, message: data.msg });
		}
	});
};

exports.productCreateWebhook = function(req, res) {
	Model.productCreateWebhookModel(req, function(err, data) {
		if (err)  res.send(err);
		if(data && data.success){
			res.status(200).send({ success:true, message: data.msg, data: data.data });
		}			
		else{
			res.status(200).send({ success:false, message: data.msg });
		}
	});
};

exports.productDeleteWebhook = function(req, res) {
	Model.productDeleteWebhookModel(req, function(err, data) {
		if (err)  res.send(err);
		if(data && data.success){
			res.status(200).send({ success:true, message: data.msg, data: data.data });
		}			
		else{
			res.status(200).send({ success:false, message: data.msg });
		}
	});
};

exports.productUpdateWebhook = function(req, res) {
	Model.productUpdateWebhookModel(req, function(err, data) {
		if (err)  res.send(err);
		if(data && data.success){
			res.status(200).send({ success:true, message: data.msg, data: data.data });
		}			
		else{
			res.status(200).send({ success:false, message: data.msg });
		}
	});
};

exports.orderCreateWebhook = function(req, res) {
	Model.orderCreateWebhookModel(req, function(err, data) {
		if (err)  res.send(err);
		if(data && data.success){
			res.status(200).send({ success:true, message: data.msg, data: data.data });
		}			
		else{
			res.status(200).send({ success:false, message: data.msg });
		}
	});
};

exports.orderDeleteWebhook = function(req, res) {
	Model.orderDeleteWebhookModel(req, function(err, data) {
		if (err)  res.send(err);
		if(data && data.success){
			res.status(200).send({ success:true, message: data.msg, data: data.data });
		}			
		else{
			res.status(200).send({ success:false, message: data.msg });
		}
	});
};

exports.orderUpdateWebhook = function(req, res) {
	Model.orderUpdateWebhookModel(req, function(err, data) {
		if (err)  res.send(err);
		if(data && data.success){
			res.status(200).send({ success:true, message: data.msg, data: data.data });
		}			
		else{
			res.status(200).send({ success:false, message: data.msg });
		}
	});
};