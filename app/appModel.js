'user strict';
const util 		= require('util');
var sql 		= require('./db');
var dateTime 	= require('node-datetime');
var async 	    = require("async");
const multer 	= require('multer');
const path 		= require('path');
var fs 			= require('fs');
const nodemailer = require('nodemailer');
var helper 		= require('./helpers');
var moment = require('moment');

const Shopify = require('shopify-api-node');

const shopify = new Shopify({
	shopName: process.env.SHOPIFY_NAME,
	apiKey: process.env.SHOPIFY_API_KEY,
	password: process.env.SHOPIFY_PASSWORD,
	apiVersion:'2020-04',
	autoLimit: true
});

//Model object constructor
var Model = function(object){
    this.created_at = new Date();
};

Model.getShopifyProductsModel =  async function getShopifyProductsModel(req, result) {

	var param   = req.body
	var store_id = (param.store_id) ? param.store_id : 1;
	var response_arr  = [];

	var new_data = [];
	var new_data1 = [];
	(async () => {
		let params = { limit: 50 };
		console.log("Count:",await shopify.product.count());
		var counter = 1;
		do {
		  const products = await shopify.product.list(params);
		  new_data = new_data.concat(products);
		  params = products.nextPageParameters;
		  console.log("params:",params, "Counter:",counter);
		  counter++;
		} while (params !== undefined);

		console.log('new_data:',new_data.length);

		for (const product of new_data1) {
		
			// Check Product Exist Or not
			var query_text = 'SELECT id FROM products WHERE store_product_id = ?';
			sql.query( query_text, [product.id], ( err, rows ) => {
				if (err) throw err;
				
				if(rows.length == 0){
		
					// Add data in Product  Table
					var set_data = {
						"store_id":store_id,
						"store_product_id":product.id,
						"title":product.title,
						"type":product.type,
						"tags":product.tags,
						"handle":product.handle,
						"body_html":product.body_html,
						"status":product.status,
						"vendor":product.vendor,
						"published_at":product.published_at,
						"created_at":product.created_at,
						"updated_at":product.updated_at

					}
					// console.log("set data:",set_data);
					var query_text = 'INSERT INTO products SET ?';
					sql.query( query_text, [set_data], ( err, rows ) => {
						if (err) throw err;
						console.log("rows insertId:",rows.insertId);
		
						if(rows.insertId > 0){
		
							// Add Records to Variant
							if(product.variants && product.variants.length > 0){
								
								for (const variant of product.variants) {

									var set_variant = {
										"title":variant.title,
										"wave_image_id":0,
										"price":variant.price,
										"compare_at_price":variant.compare_at_price,
										"body_html":'',
										"status":0,
										"weight":variant.weight,	
										"weight_unit":variant.weight_unit,
										"sku":variant.sku,
										"quantity":0,
										"position":variant.position,
										"barcode":variant.barcode,
										"wave_product_id":rows.insertId,
										"options":'',
										"inventory_item_id":variant.inventory_item_id,
										"inventory_management":variant.inventory_management,
										"inventory_policy":variant.inventory_policy,
										"published_at":'',
										"created_at":variant.created_at,
										"updated_at":variant.updated_at
									}

									console.log("set_variant:",set_variant);
								}
							}
		
							// Add Records to Images
							if(product.images && product.images.length > 0){
								
							}
		
		
						}
					});
		
				}
			});
		}

		response_arr['success']  = 1;	
		response_arr['msg']      = "Product fetched successfully!";	
		response_arr['data']	 = new_data;
		result(null, response_arr);
		
	})().catch(console.error);

};

Model.getShopifyCustomersModel =  async function getShopifyCustomersModel(req, result) {

	var param    = req.body
	var store_id = (param.store_id) ? param.store_id : 1;
	var response_arr  = [];

	var new_data = [];
	(async () => {
		let params = { limit: 50 };
		console.log("Count:",await shopify.customer.count());
		var counter = 1;
		do {
		  const customers = await shopify.customer.list(params);

		  new_data = new_data.concat(customers);
		  params = customers.nextPageParameters;
		  console.log("params:",params, "Counter:",counter);
		  counter++;
		} while (params !== undefined);

		console.log('new_data:',new_data.length);

		for (const customer of new_data) {
		
			// Check Customer Exist Or not
			var query_text = 'SELECT id FROM customers WHERE store_customer_id = ?';
			sql.query( query_text, [customer.id], ( err, rows ) => {
				if (err) throw err;
				
				if(rows.length == 0){
		
					// Add data in Customer Table
					var set_data = {
						"store_id":store_id,
						"store_customer_id":(customer.id) ? customer.id : '',
						"first_name":(customer.first_name) ? customer.first_name : '',
						"last_name":(customer.last_name) ? customer.last_name : '',
						"email":(customer.email) ? customer.email : '',
						"phone":(customer.phone) ? customer.phone : '',
						"status":(customer.state) ? customer.state : '',
						"total_spent":(customer.total_spent) ? customer.total_spent : 0,
						"tags":(customer.tags) ? customer.tags : '',
						"note":(customer.note) ? customer.note : '',
						"order_counts":(customer.order_counts) ? customer.order_counts : 0,
						"last_order_id":(customer.last_order_id) ? customer.last_order_id : '',
						"verified_email":(customer.verified_email) ? customer.verified_email : '',
						"accepts_marketings":(customer.accepts_marketings) ? customer.accepts_marketings : '',
						"accepts_marketing_updated_at":(customer.accepts_marketing_updated_at) ? customer.accepts_marketing_updated_at : '',
						"tax_exemption":(customer.tax_exemption) ? customer.tax_exemption : '',
						"tax_exemption_id":(customer.tax_exemption_id) ? customer.tax_exemption_id : '',
						"created_at":customer.created_at,
						"updated_at":customer.updated_at
					}
					console.log('customer set:',set_data);
					var query_text = 'INSERT INTO customers SET ?';
					sql.query( query_text, [set_data], ( err, rows ) => {
						if (err) throw err;
						console.log("rows insertId:",rows.insertId);
		
						if(rows.insertId > 0){
		
							// Add Records to Address
							if(customer.addresses && customer.addresses.length > 0){
								
								for (const address of customer.addresses) {
									var set_data2 = {
										"first_name":(address.first_name) ? address.first_name : '',
										"last_name":(address.last_name) ? address.last_name : '',
										"phone":(address.phone) ? address.phone : '',
										"address1":(address.address1) ? address.address1 : '',
										"address2":(address.address2) ? address.address2 : '',
										"city":(address.city) ? address.city : '',
										"state":(address.state) ? address.state : '',
										"country":(address.country) ? address.country  : '',
										"country_code":(address.country_code) ? address.country_code : '',
										"company_name":(address.company) ? address.company : '',
										"zip":(address.zip) ? address.zip : '',
										"is_default":(address.default) ? address.default : '',
										"is_shipping":0,
										"is_billing":0,
										"wave_customer_id":rows.insertId,
										"created_at":moment().format('YYYY-MM-DD HH:mm:ss'),
										"updated_at":moment().format('YYYY-MM-DD HH:mm:ss'),
									}
									console.log("address:",set_data2);
									// var query_text = 'INSERT INTO addresses SET ?';
									// sql.query( query_text, [set_data2], ( err, rows ) => {
									// 	if (err) throw err;
									// });
								}
							}
						}
					});
				}
			});
		}
		response_arr['success']  = 1;	
		response_arr['msg']      = "Customer fetched successfully!";	
		response_arr['data']	 = new_data;
		result(null, response_arr);
		
	})().catch(console.error);

};

Model.getShopifyCollectionsModel =  async function getShopifyCollectionsModel(req, result) {

	var param    = req.body
	var store_id = (param.store_id) ? param.store_id : 1;
	var response_arr  = [];

	var new_data = [];
	(async () => {
		let params = { limit: 50 };
		// console.log("Count:",await shopify.collectionListing.count());
		var counter = 1;
		do {
		  const collections = await shopify.collectionListing.list(params);

		  new_data = new_data.concat(collections);
		  params = collections.nextPageParameters;
		  console.log("params:",params, "Counter:",counter);
		  counter++;
		} while (params !== undefined);

		console.log('new_data:',new_data.length);

		for (const collection of new_data) {
			const collections_data = await shopify.collection.products(collection.collection_id);
			console.log("collection_id:",collection.collection_id," | collections_data:",collections_data);
		}
	
		response_arr['success']  = 1;	
		response_arr['msg']      = "Collection fetched successfully!";	
		response_arr['data']	 = new_data;
		result(null, response_arr);
		
	})().catch(console.error);

};

Model.getShopifyOrdersModel =  async function getShopifyOrdersModel(req, result) {

	var param    = req.body
	var store_id = (param.store_id) ? param.store_id : 1;
	var response_arr  = [];

	var new_data = [];
	(async () => {
		let params = { limit: 50 };
		console.log("Count:",await shopify.order.count());
		var counter = 1;
		do {
		  const orders = await shopify.order.list(params);

		  new_data = new_data.concat(orders);
		  params = orders.nextPageParameters;
		  console.log("params:",params, "Counter:",counter);
		  counter++;
		} while (params !== undefined);

		console.log('new_data:',new_data.length);
		for (const order of new_data) {

			var order_set = {
				"billing_address_id": order.id,	
				"shipping_address_id": order.id,	
				"checkout_token"	: order.id,
				"cart_token"	: order.id,
				"order_token"	: order.id,
				"status"	: order.id,
				"quantity"	: order.id,
				"financial_status"	: order.id,
				"note"	: order.id,
				"cancel_reason"	: order.id,
				"discount_id"	: order.id,
				"refund_id"	: order.id,
				"total_items_price"	: order.id,
				"total_discounts"	: order.id,
				"subtotal_price"	: order.id,
				"total_shipping_price"	: order.id,
				"total_tax"	: order.id,
				"total_price"	: order.id,
				"total_weight"	: order.id,
				"order_status_url"	: order.id,
				"referring_site"	: order.id,
				"processing_method"	: order.id,
				"source_name"	: order.id,
				"fulfillment_status"	: order.id,
				"buyer_accepts_marketing"	: order.id,
				"app_id"	: order.id,
				"browser_ip"	: order.id,
				"wave_customer_id"	: order.id,
				"processed_at"	: order.id,
				"closed_at"	: order.id,
				"cancelled_at"	: order.id,
				"created_at"	: order.id,
				"updated_at"	: order.id,
			};
			
		}
	
		response_arr['success']  = 1;	
		response_arr['msg']      = "Orders fetched successfully!";	
		response_arr['data']	 = new_data;
		result(null, response_arr);
		
	})().catch(console.error);

};

Model.customerUpdateWebhookModel =  async function customerUpdateWebhookModel(req, result) {
	var response_arr  = [];
	console.log('Customer Update webhook');
	(async () => {
		var customer = req.body;
		console.log(customer) // Call your action on the request here
		var customer_id = customer.id;

		var query_text = `SELECT id FROM customers WHERE store_customer_id = '${customer_id}'`;
		sql.query( query_text, [], ( err, rows ) => {
			if (err) throw err;
			if(rows.length > 0){

				// Update data in Customer Table
				var set_data = {
					// "store_id":store_id,
					// "store_customer_id":(customer.id) ? customer.id : '',
					"first_name":(customer.first_name) ? customer.first_name : '',
					"last_name":(customer.last_name) ? customer.last_name : '',
					"email":(customer.email) ? customer.email : '',
					"phone":(customer.phone) ? customer.phone : '',
					"status":(customer.state) ? customer.state : '',
					"total_spent":(customer.total_spent) ? customer.total_spent : 0,
					"tags":(customer.tags) ? customer.tags : '',
					"note":(customer.note) ? customer.note : '',
					"order_counts":(customer.order_counts) ? customer.order_counts : 0,
					"last_order_id":(customer.last_order_id) ? customer.last_order_id : '',
					"verified_email":(customer.verified_email) ? customer.verified_email : '',
					"accepts_marketings":(customer.accepts_marketings) ? customer.accepts_marketings : '',
					"accepts_marketing_updated_at":(customer.accepts_marketing_updated_at) ? customer.accepts_marketing_updated_at : '',
					"tax_exemption":(customer.tax_exempt) ? customer.tax_exempt : '',
					// "tax_exemption_id":(customer.tax_exemption_id) ? customer.tax_exemption_id : '',
					"created_at":customer.created_at,
					"updated_at":customer.updated_at
				}
				console.log('customer set:',set_data);
				var query_text = 'UPDATE customers SET ? WHERE id = ?';
				sql.query( query_text, [set_data,rows[0]['id']], ( err, rows ) => {
					if (err) throw err;

					// Add Records to Address
					if(customer.addresses && customer.addresses.length > 0){
								
						// for (const address of customer.addresses) {
						// 	var set_data2 = {
						// 		"first_name":(address.first_name) ? address.first_name : '',
						// 		"last_name":(address.last_name) ? address.last_name : '',
						// 		"phone":(address.phone) ? address.phone : '',
						// 		"address1":(address.address1) ? address.address1 : '',
						// 		"address2":(address.address2) ? address.address2 : '',
						// 		"city":(address.city) ? address.city : '',
						// 		"state":(address.state) ? address.state : '',
						// 		"country":(address.country) ? address.country  : '',
						// 		"country_code":(address.country_code) ? address.country_code : '',
						// 		"company_name":(address.company) ? address.company : '',
						// 		"zip":(address.zip) ? address.zip : '',
						// 		"is_default":(address.default) ? address.default : '',
						// 		"is_shipping":0,
						// 		"is_billing":0,
						// 		"wave_customer_id":rows[0]['id'],
						// 		"created_at":moment().format('YYYY-MM-DD HH:mm:ss'),
						// 		"updated_at":moment().format('YYYY-MM-DD HH:mm:ss'),
						// 	}
						// 	console.log("address:",set_data2);
						// 	var query_text = 'SELECT id FROM addresses';
						// 	sql.query( query_text, [set_data2], ( err, rows ) => {
						// 		if (err) throw err;
						// 	});
						// }
					}

				});
			}
			else{

			}
		});
		result(null, customer);
	})().catch(console.error);
};

Model.customerCreateWebhookModel =  async function customerCreateWebhookModel(req, result) {
	var response_arr  = [];
	console.log('Customer Create webhook');
	// var store_id = (param.store_id) ? param.store_id : 1;
	var store_id = 1;
	(async () => {
		var customer = req.body;
		console.log(customer) // Call your action on the request here
		var customer_id = customer.id;

		// Add data in Customer Table
		var set_data = {
			"store_id":store_id,
			"store_customer_id":(customer.id) ? customer.id : '',
			"first_name":(customer.first_name) ? customer.first_name : '',
			"last_name":(customer.last_name) ? customer.last_name : '',
			"email":(customer.email) ? customer.email : '',
			"phone":(customer.phone) ? customer.phone : '',
			"status":(customer.state) ? customer.state : '',
			"total_spent":(customer.total_spent) ? customer.total_spent : 0,
			"tags":(customer.tags) ? customer.tags : '',
			"note":(customer.note) ? customer.note : '',
			"order_counts":(customer.order_counts) ? customer.order_counts : 0,
			"last_order_id":(customer.last_order_id) ? customer.last_order_id : '',
			"verified_email":(customer.verified_email) ? customer.verified_email : '',
			"accepts_marketings":(customer.accepts_marketings) ? customer.accepts_marketings : '',
			"accepts_marketing_updated_at":(customer.accepts_marketing_updated_at) ? customer.accepts_marketing_updated_at : '',
			"tax_exemption":(customer.tax_exempt) ? customer.tax_exempt : '',
			"tax_exemption_id":(customer.tax_exemption_id) ? customer.tax_exemption_id : '',
			"created_at":customer.created_at,
			"updated_at":customer.updated_at
		}
		console.log('customer set:',set_data);
		var query_text = 'INSERT INTO customers SET ?';
		sql.query( query_text, [set_data], ( err, rows ) => {
			if (err) throw err;
			console.log("rows insertId:",rows.insertId);

			if(rows.insertId > 0){

				// Add Records to Address
				if(customer.addresses && customer.addresses.length > 0){
					
					for (const address of customer.addresses) {
						var set_data2 = {
							"first_name":(address.first_name) ? address.first_name : '',
							"last_name":(address.last_name) ? address.last_name : '',
							"phone":(address.phone) ? address.phone : '',
							"address1":(address.address1) ? address.address1 : '',
							"address2":(address.address2) ? address.address2 : '',
							"city":(address.city) ? address.city : '',
							"state":(address.state) ? address.state : '',
							"country":(address.country) ? address.country  : '',
							"country_code":(address.country_code) ? address.country_code : '',
							"company_name":(address.company) ? address.company : '',
							"zip":(address.zip) ? address.zip : '',
							"is_default":(address.default) ? address.default : '',
							"is_shipping":0,
							"is_billing":0,
							"wave_customer_id":rows.insertId,
							"created_at":moment().format('YYYY-MM-DD HH:mm:ss'),
							"updated_at":moment().format('YYYY-MM-DD HH:mm:ss'),
						}
						console.log("address:",set_data2);
						var query_text = 'INSERT INTO addresses SET ?';
						sql.query( query_text, [set_data2], ( err, rows ) => {
							if (err) throw err;
						});
					}
				}
			}
		});
	
		result(null, customer);
	})().catch(console.error);
};

Model.customerDeleteWebhookModel =  async function customerDeleteWebhookModel(req, result) {
	var response_arr  = [];
	console.log('Customer Delete webhook');
	(async () => {
		var customer = req.body;
		console.log(customer);
		result(null, customer);
	})().catch(console.error);
};

Model.collectionCreateWebhookModel =  async function collectionCreateWebhookModel(req, result) {
	var response_arr  = [];
	console.log('Collection Create webhook');
	(async () => {
		var customer = req.body;
		console.log(customer);
		result(null, customer);
	})().catch(console.error);
};

Model.collectionUpdateWebhookModel =  async function collectionUpdateWebhookModel(req, result) {
	var response_arr  = [];
	console.log('Collection Update webhook');
	(async () => {
		var customer = req.body;
		console.log(customer);
		result(null, customer);
	})().catch(console.error);
};

Model.collectionDeleteWebhookModel =  async function collectionDeleteWebhookModel(req, result) {
	var response_arr  = [];
	console.log('Collection Delete webhook');
	(async () => {
		var customer = req.body;
		console.log(customer);
		result(null, customer);
	})().catch(console.error);
};

Model.productCreateWebhookModel =  async function productCreateWebhookModel(req, result) {
	var response_arr  = [];
	console.log('Product Create webhook');
	(async () => {
		var customer = req.body;
		console.log(customer);
		result(null, customer);
	})().catch(console.error);
};

Model.productDeleteWebhookModel =  async function productDeleteWebhookModel(req, result) {
	var response_arr  = [];
	console.log('Product Delete webhook');
	(async () => {
		var customer = req.body;
		console.log(customer);
		result(null, customer);
	})().catch(console.error);
};

Model.productUpdateWebhookModel =  async function productUpdateWebhookModel(req, result) {
	var response_arr  = [];
	console.log('Product Update webhook');
	(async () => {
		var customer = req.body;
		console.log(customer);
		result(null, customer);
	})().catch(console.error);
};

Model.orderCreateWebhookModel =  async function orderCreateWebhookModel(req, result) {
	var response_arr  = [];
	console.log('Order Create webhook');
	(async () => {
		var customer = req.body;
		console.log(customer);
		result(null, customer);
	})().catch(console.error);
};

Model.orderDeleteWebhookModel =  async function orderDeleteWebhookModel(req, result) {
	var response_arr  = [];
	console.log('Order Delete webhook');
	(async () => {
		var customer = req.body;
		console.log(customer);
		result(null, customer);
	})().catch(console.error);
};

Model.orderUpdateWebhookModel =  async function orderUpdateWebhookModel(req, result) {
	var response_arr  = [];
	console.log('Order Update webhook');
	(async () => {
		var customer = req.body;
		console.log(customer);
		result(null, customer);
	})().catch(console.error);
};

module.exports= Model;