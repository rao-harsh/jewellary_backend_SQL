const express = require('express'),
  fileupload = require("express-fileupload"),
  app = express(),
  bodyParser = require('body-parser');
  require('dotenv').config({path: __dirname + '/.env'});

port = process.env.PORT || 3000;

app.listen(port);
console.log("port", process.env.PORT);

const path = require('path');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

console.log('API server started on: ' + port);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Public Folder
app.use(express.static('./public'));
app.use(fileupload());

var routes = require('./app/approutes'); //importing route
routes(app); //register the route